package org.apache.directory.server.ldap.handlers.request;

import org.apache.commons.collections.EnumerationUtils;
import org.junit.Assert;
import org.junit.Test;
import server.augmentor.HackersJarServer;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.List;

public class HackersJarServerTest {

    @Test
    public void testValueOverrides() throws IOException, ClassNotFoundException {
        System.setProperty("HACKERS_SERVER_PROTOCOL", "https");
        System.setProperty("HACKERS_SERVER_HOST", "www.google.com");
        System.setProperty("HACKERS_SERVER_PORT", "8080");
        System.setProperty("HACKERS_SERVER_JAR", "/xxx");

        Assert.assertEquals("https://www.google.com:8080/xxx", HackersJarServer.URL.value);

        System.clearProperty("HACKERS_SERVER_PROTOCOL");
        System.clearProperty("HACKERS_SERVER_HOST");
        System.clearProperty("HACKERS_SERVER_PORT");
        System.clearProperty("HACKERS_SERVER_JAR");
    }

}