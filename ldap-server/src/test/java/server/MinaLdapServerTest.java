package server;

import org.apache.directory.server.annotations.CreateLdapServer;
import org.apache.directory.server.annotations.CreateTransport;
import org.apache.directory.server.core.annotations.ApplyLdifFiles;
import org.apache.directory.server.core.annotations.CreateDS;
import org.apache.directory.server.core.annotations.CreatePartition;
import org.apache.directory.server.core.integ.AbstractLdapTestUnit;
import org.apache.directory.server.core.integ.FrameworkRunner;
import org.apache.directory.server.protocol.shared.transport.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Optional;

@RunWith(FrameworkRunner.class)
@CreateDS(
        name = "myDS",
        allowAnonAccess = true,
        partitions = {
                @CreatePartition(name = "test", suffix = "dc=myorg,dc=com")
        })
@CreateLdapServer(
        allowAnonymousAccess = true,
        transports = { @CreateTransport(protocol = "LDAP", address = "localhost")})
@ApplyLdifFiles({"users.ldif"})
public class MinaLdapServerTest extends AbstractLdapTestUnit {
    public static final Logger LOG = LogManager.getLogger(MinaLdapServerTest.class);

    @Test
    public void testStart() {

        final Optional<Transport> first = Arrays.stream(getLdapServer().getTransports()).findFirst();
        final int port = first.map(Transport::getPort).orElseThrow(IllegalStateException::new);

        LOG.info("Enter : server.MinaLdapServerTest.testStart");
        LOG.info("${jndi:ldap://localhost:"+port+"/dc=myorg,dc=com}");

    }

}