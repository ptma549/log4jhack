package server.config;

import org.apache.directory.server.core.annotations.ApplyLdifFiles;

import java.lang.annotation.Annotation;

public class ApplyLdifFilesImpl implements ApplyLdifFiles {
    @Override
    public String[] value() {
        return new String[] {"users.ldif"};
    }

    @Override
    public Class<?> clazz() {
        return ApplyLdifFiles.class;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return ApplyLdifFiles.class;
    }
}
