package server.config;

import org.apache.directory.server.annotations.CreateTransport;
import org.apache.directory.server.annotations.TransportType;

import java.lang.annotation.Annotation;

public class CreateTransportImpl implements CreateTransport {
    private final int port = Integer.getInteger("LDAP_SERVER_PORT", 7389);

    @Override
    public String protocol() {
        return "LDAP";
    }

    @Override
    public TransportType type() {
        return TransportType.TCP;
    }

    @Override
    public int port() {
        return port;
    }

    @Override
    public String address() {
        return "localhost";
    }

    @Override
    public int backlog() {
        return 50;
    }

    @Override
    public boolean ssl() {
        return false;
    }

    @Override
    public int nbThreads() {
        return 3;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }
}
