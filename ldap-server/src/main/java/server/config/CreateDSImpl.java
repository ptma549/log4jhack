package server.config;

import org.apache.directory.server.core.annotations.CreateAuthenticator;
import org.apache.directory.server.core.annotations.CreateDS;
import org.apache.directory.server.core.annotations.CreatePartition;
import org.apache.directory.server.core.annotations.LoadSchema;
import org.apache.directory.server.core.factory.DefaultDirectoryServiceFactory;

import java.lang.annotation.Annotation;

public class CreateDSImpl implements CreateDS {
    @Override
    public Class<?> factory() {
        return DefaultDirectoryServiceFactory.class;
    }

    @Override
    public String name() {
        return "myDS";
    }

    @Override
    public boolean enableAccessControl() {
        return false;
    }

    @Override
    public boolean allowAnonAccess() {
        return false;
    }

    @Override
    public boolean enableChangeLog() {
        return true;
    }

    @Override
    public CreatePartition[] partitions() {
        return new CreatePartition[] {new CreatePartitionImpl()};
    }

    @Override
    public Class<?>[] additionalInterceptors() {
        return new Class[0];
    }

    @Override
    public CreateAuthenticator[] authenticators() {
        return new CreateAuthenticator[0];
    }

    @Override
    public LoadSchema[] loadedSchemas() {
        return new LoadSchema[0];
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return CreateDS.class;
    }
}
