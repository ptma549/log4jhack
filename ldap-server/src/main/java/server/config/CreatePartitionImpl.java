package server.config;

import org.apache.directory.server.core.annotations.ContextEntry;
import org.apache.directory.server.core.annotations.CreateIndex;
import org.apache.directory.server.core.annotations.CreatePartition;
import org.apache.directory.server.core.api.partition.Partition;

import java.lang.annotation.Annotation;

public class CreatePartitionImpl implements CreatePartition {
    @Override
    public Class<? extends Partition> type() {
        return Partition.class;
    }

    @Override
    public String name() {
        return "test";
    }

    @Override
    public String suffix() {
        return "dc=myorg,dc=com";
    }

    @Override
    public ContextEntry contextEntry() {
        return new ContextEntry(){
            @Override
            public Class<? extends Annotation> annotationType() {
                return null;
            }

            @Override
            public String entryLdif() {
                return "";
            }
        };
    }

    @Override
    public CreateIndex[] indexes() {
        return new CreateIndex[0];
    }

    @Override
    public int cacheSize() {
        return 1000;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }
}
