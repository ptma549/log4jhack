package server.config;

import org.apache.directory.server.annotations.CreateLdapServer;
import org.apache.directory.server.annotations.CreateTransport;
import org.apache.directory.server.annotations.SaslMechanism;

import java.lang.annotation.Annotation;

public class CreateLdapServerImpl implements CreateLdapServer {

    @Override
    public String name() {
        return "DefaultLdapServer";
    }

    @Override
    public CreateTransport[] transports() {
        final CreateTransportImpl createTransport = new CreateTransportImpl();
        return new CreateTransport[] {createTransport};
    }

    @Override
    public Class<?> factory() {
        return org.apache.directory.server.factory.DefaultLdapServerFactory.class;
    }

    @Override
    public long maxSizeLimit() {
        return 1000;
    }

    @Override
    public int maxTimeLimit() {
        return 1000;
    }

    @Override
    public boolean allowAnonymousAccess() {
        return true;
    }

    @Override
    public String keyStore() {
        return "";
    }

    @Override
    public String certificatePassword() {
        return "";
    }

    @Override
    public Class<?>[] extendedOpHandlers() {
        return new Class[0];
    }

    @Override
    public SaslMechanism[] saslMechanisms() {
        return new SaslMechanism[0];
    }

    @Override
    public Class<?> ntlmProvider() {
        return Object.class;
    }

    @Override
    public String saslHost() {
        return "ldap.example.com";
    }

    @Override
    public String[] saslRealms() {
        return new String[] {"example.com"};
    }

    @Override
    public String saslPrincipal() {
        return "ldap/ldap.example.com@EXAMPLE.COM";
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return CreateLdapServer.class;
    }
}
