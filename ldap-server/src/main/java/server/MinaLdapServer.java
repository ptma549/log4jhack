package server;

import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.server.annotations.CreateLdapServer;
import org.apache.directory.server.constants.ServerDNConstants;
import org.apache.directory.server.core.annotations.ApplyLdifFiles;
import org.apache.directory.server.core.api.DirectoryService;
import org.apache.directory.server.core.factory.DSAnnotationProcessor;
import org.apache.directory.server.core.security.TlsKeyGenerator;
import org.apache.directory.server.factory.ServerAnnotationProcessor;
import org.apache.directory.server.ldap.LdapServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.config.ApplyLdifFilesImpl;
import server.config.CreateDSImpl;
import server.config.CreateLdapServerImpl;


public class MinaLdapServer {
    private static final Logger log = LoggerFactory.getLogger(MinaLdapServer.class);

    public static void main(final String[] args) {
        new MinaLdapServer();
    }

    public MinaLdapServer() {
        init();
    }

    private void init() {
        try {
            final CreateDSImpl createDS = new CreateDSImpl();
            final DirectoryService directoryService = DSAnnotationProcessor.createDS(createDS);

            final String[] ldifFilenames = new ApplyLdifFilesImpl().value();
            DSAnnotationProcessor.injectLdifFiles(ApplyLdifFiles.class, directoryService, ldifFilenames);

            updateTlsKey(directoryService);

            final CreateLdapServer classLdapServerBuilder = new CreateLdapServerImpl();
            final LdapServer classLdapServer = ServerAnnotationProcessor.instantiateLdapServer(classLdapServerBuilder, directoryService);

            classLdapServer.start();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }
    }


    private void updateTlsKey( DirectoryService ds ) throws LdapException, LdapInvalidDnException
    {
        // Update TLS key for tests. Newer Java 8 releases consider RSA keys
        // with less than 1024 bits as insecure and such are disabled by default, see
        // http://www.oracle.com/technetwork/java/javase/8-compatibility-guide-2156366.html
        Entry adminEntry = ds.getAdminSession().lookup( new Dn( ServerDNConstants.ADMIN_SYSTEM_DN ) );
        TlsKeyGenerator.addKeyPair( adminEntry, TlsKeyGenerator.CERTIFICATE_PRINCIPAL_DN,
                TlsKeyGenerator.CERTIFICATE_PRINCIPAL_DN, "RSA", 1024 );
        Modification mod1 = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE,
                adminEntry.get( TlsKeyGenerator.PRIVATE_KEY_AT ) );
        Modification mod2 = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE,
                adminEntry.get( TlsKeyGenerator.PUBLIC_KEY_AT ) );
        Modification mod3 = new DefaultModification( ModificationOperation.REPLACE_ATTRIBUTE,
                adminEntry.get( TlsKeyGenerator.USER_CERTIFICATE_AT ) );
        ds.getAdminSession().modify( adminEntry.getDn(), mod1, mod2, mod3 );
    }


}
