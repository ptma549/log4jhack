package server.augmentor;

import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public enum HackersJarServer {

    URL;

    public final String value;

    private HackersJarServer() {
        final String protocol = System.getProperty("HACKERS_SERVER_PROTOCOL", "http");
        final String host = System.getProperty("HACKERS_SERVER_HOST", "localhost");
        final Integer port = Integer.getInteger("HACKERS_SERVER_PORT", 9090);
        final String filename = System.getProperty("HACKERS_SERVER_JAR", "hacker-code-1.0-SNAPSHOT.jar");
        final String filenamePrefix = StringUtils.startsWith(filename,"/") ? "" : "/";
        try {
            value = new URL(
                    protocol,
                    host,
                    port,
                    filenamePrefix + filename
            ).toURI().toASCIIString();
        } catch (URISyntaxException | MalformedURLException e) {
            final String message = String.format("failed to construct URL with params:" +
                            "protocol=\"%s\"; " +
                            "host=\"%s\"; " +
                            "port=\"%s\"; " +
                            "filename=\"%s\"; ",
                    protocol, host, port, filename);
            throw new RuntimeException(message, e);
        }
    }

}
