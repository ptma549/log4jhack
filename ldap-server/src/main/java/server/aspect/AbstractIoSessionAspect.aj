package server.aspect;

import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.core.future.WriteFuture;

public aspect AbstractIoSessionAspect {

    pointcut callInfo(Object message, AbstractIoSession session) :
     call(WriteFuture write(Object)) && args(message) && target(session);

    before(Object message, AbstractIoSession session) : callInfo(message, session) {
      IoSessionAspectProcessor.before(message, session);
    }

    WriteFuture around(Object message, AbstractIoSession session) :
      callInfo(message, session) {
        IoSessionAspectProcessor.aroundBefore(message, session);
        WriteFuture result = proceed(message, session);
        IoSessionAspectProcessor.aroundAfter(message, session);
        return result;
    }

    after(Object message, AbstractIoSession session) : callInfo(message, session) {
      IoSessionAspectProcessor.after(message, session);
    }
}