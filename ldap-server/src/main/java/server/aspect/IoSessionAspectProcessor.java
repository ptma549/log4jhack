package server.aspect;

import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchResultEntry;
import org.apache.directory.api.ldap.model.message.SearchResultEntryImpl;
import server.augmentor.HackersCodeAugmentor;
import org.apache.mina.core.session.AbstractIoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class IoSessionAspectProcessor {
    private static final Logger log = LoggerFactory.getLogger(IoSessionAspectProcessor.class);

    public static void before(Object message, AbstractIoSession session) {
        log.info("before: ({} :: {}, {})", message.getClass(), message, session);

        if (SearchResultEntryImpl.class.equals(message.getClass())) {
            addHackersCode((SearchResultEntry)message, session);
        }
    }

    private static void addHackersCode(SearchResultEntry message, AbstractIoSession session) {
        try {
            HackersCodeAugmentor.add(message, "");
        } catch (LdapException | IOException e) {
            log.error("failed to augment message with hackers code", e);
        }
    }

    public static void after(Object message, AbstractIoSession session) {
        log.debug("after: ({}, {})", message, session);
    }

    public static void aroundBefore(Object message, AbstractIoSession session) {
        log.debug("aroundBefore: ({}, {})", message, session);
    }

    public static void aroundAfter(Object message, AbstractIoSession session) {
        log.debug("aroundAfter: ({}, {})", message, session);
    }
}
