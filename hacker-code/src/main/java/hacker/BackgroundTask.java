package hacker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BackgroundTask implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(BackgroundTask.class);

    private final String value;
    private final int delay;

    public BackgroundTask(final String value, final int delay) {
        this.value = value;
        this.delay = delay;
    }

    @Override
    public void run() {
        log.warn("Entering run method on thread : {}", Thread.currentThread());
        int loopIndex = 1;
        while (true) {
            runBackgroundMethod(value, loopIndex++);
        }
    }

    private void runBackgroundMethod(String value, int loopIndex) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.warn("loop[{}]  *** message from ldap-server *** : '{}'", loopIndex, value);
    }

}
