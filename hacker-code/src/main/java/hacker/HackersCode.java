package hacker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Objects.isNull;

public class HackersCode implements Serializable {

    private static final Logger log = LoggerFactory.getLogger(HackersCode.class);

    private static final long serialVersionUID = 953948690288182100L;
    private static final int DELAY = 3000;
    private static final AtomicInteger objectIndex = new AtomicInteger(1);

    static {
        log.warn("HackersCode : inside static initializer");
    }

    private final String value;

    public HackersCode(final String value) {
        this.value = defaultIfBlank(value);
    }

    public String getValue() {
        return value;
    }

    private void runBackgroundTask(String threadId, String value) {
        final BackgroundTask backgroundTask = new BackgroundTask(value, DELAY);
        final Thread backgroundThread = new Thread(backgroundTask, "HACKERS-THREAD-" + threadId);
        backgroundThread.start();
    }

    private Object readResolve() throws ObjectStreamException {
        log.warn("HackersCode : inside readResolve");
        final String threadId = defaultIfBlank(getValue());
        this.runBackgroundTask(threadId, getValue());
        return this;
    }

    private boolean valueIsBlank(final String value) {
        return isNull(value) || "".equals(value.trim());
    }

    private String defaultIfBlank(final String value) {
        if (valueIsBlank(value)) {
            return ""+objectIndex.getAndAdd(1);
        } else {
            return value;
        }
    }

    public byte[] toByteArray() throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new ObjectOutputStream(baos).writeObject(this);
        return baos.toByteArray();
    }

    public static HackersCode fromByteArray(byte[] bytes) throws IOException, ClassNotFoundException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        final ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream);
        return (HackersCode) ois.readObject();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HackersCode that = (HackersCode) o;

        return value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
