import hacker.HackersCode;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class HackersCodeTest {

    @Test
    public void testSerializationDeserialization() throws IOException, ClassNotFoundException {
        HackersCode hackersCode = new HackersCode("1234");
        final byte[] bytes = hackersCode.toByteArray();

        final HackersCode deserializedHackersCode = HackersCode.fromByteArray(bytes);

        Assert.assertEquals("1234", deserializedHackersCode.getValue());
        Assert.assertEquals(hackersCode, deserializedHackersCode);
    }

}