# Log4j Notes

This post details how you can replicate the LOG4J defect by 
spinning up a very simple "vulnerable" web application, 
an LDAP server which is an instance of [Apache Directory](https://directory.apache.org/apacheds/) and an HTTPServer (project jar-server) to deliver a JAR to the vulnerable web application which it will execute.

## TLDR

```
git clone https://github.com/ptma549/log4jshell-example
./mvnw -f hacker-code
./mvnw -f jar-server &
./mvnw -f ldap-server &
./mvnw -f vulnerable-webapp &
```

Building the hacker-code project will create a jar and install it into your local repository as well as the `jar-server/target/classes` directory so that the jar-server HTTPServer can deliver it up to the vulnerable webapp. 


todo 
- [x] add maven wrapper
- tidyup logging
- externalise ports to maven PARENT pom for listenting and debugging
- remove unnecessary dependencies
- tidyup ldap config




## Port sockets
9090    jar-server
        ldap-server
8080    vulnerable-webapp

