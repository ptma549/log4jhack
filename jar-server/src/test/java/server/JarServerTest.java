package server;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class JarServerTest {

    @Test
    public void testJarDownload() throws IOException {
        final JarServer jarServer = new JarServer();
        jarServer.start();

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet("http://localhost:9090/xyz");
            // The underlying HTTP connection is still held by the response object
            // to allow the response content to be streamed directly from the network socket.
            // In order to ensure correct deallocation of system resources
            // the user MUST call CloseableHttpResponse#close() from a finally clause.
            // Please note that if response content is not fully consumed the underlying
            // connection cannot be safely re-used and will be shut down and discarded
            // by the connection manager.
            try (CloseableHttpResponse response1 = httpclient.execute(httpGet)) {
                final InputStream actualResponseStream = response1.getEntity().getContent();
                final InputStream expectedResponseStream = IOUtils.resourceToURL("/hacker-code-1.0-SNAPSHOT.jar").openStream();

                Assert.assertTrue(
                        IOUtils.contentEquals(actualResponseStream, expectedResponseStream)
                );
            }
        }

    }

}