package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

class JarHandler implements HttpHandler {
    private static final Logger log = LoggerFactory.getLogger(JarHandler.class);
    public static final String JAR_FILENAME = "/hacker-code-1.0-SNAPSHOT.jar";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JAVA_ARCHIVE = "application/java-archive";

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        handleResponse(httpExchange);
    }

    private void handleResponse(HttpExchange httpExchange) throws IOException {
        httpExchange.getResponseHeaders().set(CONTENT_TYPE, APPLICATION_JAVA_ARCHIVE);
        httpExchange.sendResponseHeaders(200, 0);

        try (final InputStream resourceInputStream = IOUtils.resourceToURL(JAR_FILENAME).openStream();
             final OutputStream httpOutputStream = httpExchange.getResponseBody()) {
            IOUtils.copy(resourceInputStream, httpOutputStream);
            log.debug("responded with resource : {}", JAR_FILENAME);
        } catch (IOException e) {
            log.error("if {} is missing then run the maven goal \"install\" on project hacker-code", JAR_FILENAME);
            throw e;
        }
    }

}
