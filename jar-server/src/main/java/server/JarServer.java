package server;

import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class JarServer {
    private static final Logger log = LoggerFactory.getLogger(JarServer.class);

    private static final int PORT = Integer.getInteger("JAR_SERVER_PORT", 9090);
    private static final String HOSTNAME = System.getProperty("JAR_SERVER_HOSTNAME", "localhost");

    private HttpServer server;

    public JarServer() throws IOException {
        server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);
        server.createContext("/", new JarHandler());
        server.setExecutor(threadPoolExecutor());
    }

    public void start() {
        server.start();
        final InetSocketAddress address = server.getAddress();
        final String hostName = address.getHostName();
        final int port = address.getPort();
        log.info(" Server started on {}:{}", hostName, port);
    }

    private Executor threadPoolExecutor() {
        return Executors.newFixedThreadPool(10);
    }


    public static void main(String[] args) throws Exception {
        new JarServer().start();
    }


}
