import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

class HttpPageHandler implements HttpHandler {
    private static final Logger LOG = LogManager.getLogger(HttpPageHandler.class);

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String TEXT_HTML = "text/html";
    private static final Charset CHARSET_UTF_8 = Charset.forName("UTF-8");
    private static final int OK_RESPONSE_CODE = 200;

    private final String filename;

    HttpPageHandler(final String filename) {
        this.filename = filename;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        LOG.info("path = " + httpExchange.getRequestURI().getPath());
        LOG.info("query = " + httpExchange.getRequestURI().getQuery());

        httpExchange.getResponseHeaders().set(CONTENT_TYPE, TEXT_HTML);
        final String htmlPage = createHtmlPage();
        final byte[] htmlStreamBytes = htmlPage.getBytes(CHARSET_UTF_8);
        httpExchange.sendResponseHeaders(OK_RESPONSE_CODE, htmlStreamBytes.length);

        try (final OutputStream outputStream = httpExchange.getResponseBody() ) {
            IOUtils.write(htmlStreamBytes, outputStream);
        }

        LOG.info("response complete");
    }

    private String createHtmlPage() throws IOException {
        return IOUtils.resourceToString(filename, CHARSET_UTF_8);
    }

}
