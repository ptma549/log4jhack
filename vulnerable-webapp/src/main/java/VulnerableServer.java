import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class VulnerableServer {
    private static final Logger LOG = LogManager.getLogger(VulnerableServer.class);

    private static final String HOSTNAME = "localhost";
    private static final int PORT = Integer.getInteger("VULNERABLE_SERVER_PORT", 8080);

    private static final String INDEX_HTML = "/index.html";
    public static final String KEY_TRUST_URLCODEBASE = "com.sun.jndi.ldap.object.trustURLCodebase";

    public VulnerableServer() throws IOException {
        exsureTrustUrlCodebasePropertyIsTrue();

        HttpServer server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

        server.createContext("/", new HttpPageHandler(INDEX_HTML));
        server.setExecutor(threadPoolExecutor);
        server.start();
        LOG.info(" Server started on port " + PORT);
    }

    private void exsureTrustUrlCodebasePropertyIsTrue() {
        final String trustUrlCodebaseValue = System.getProperty(KEY_TRUST_URLCODEBASE);
        LOG.info("default value of {} = {}", KEY_TRUST_URLCODEBASE, trustUrlCodebaseValue);

        final boolean trustUrlCodebase = Boolean.valueOf(trustUrlCodebaseValue);
        if (! trustUrlCodebase) {
            LOG.warn("resetting value of {} to {}}", KEY_TRUST_URLCODEBASE, Boolean.TRUE);
            System.setProperty(KEY_TRUST_URLCODEBASE, Boolean.TRUE.toString());
        }
    }

    public static void main(String[] args) throws IOException {
        new VulnerableServer();
    }

}
