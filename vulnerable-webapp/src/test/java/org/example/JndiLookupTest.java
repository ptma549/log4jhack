package org.example;

//import com.sun.jndi.ldap.LdapCtx;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.lookup.JndiLookup;
import org.apache.logging.log4j.core.net.JndiManager;
import org.apache.logging.log4j.status.StatusLogger;
import org.junit.Assert;
import org.junit.Test;

import javax.naming.NamingException;
import java.util.Objects;

public class JndiLookupTest {

    private JndiLookup classUnderTest;

//    @Test
    public void decodeObjectTest() throws NamingException {

        System.setProperty("com.sun.jndi.ldap.object.trustURLCodebase", "true");

        Object response = null;
//        final String jndiName = "ldap://localhost:6389/dc=myorg,dc=com";
        final String jndiName = "ldap://localhost:7389/dc=myorg,dc=com";
        try (final JndiManager jndiManager = JndiManager.getDefaultManager()) {
            response = jndiManager.lookup(jndiName);
        } catch (final NamingException e) {
            e.printStackTrace();
            System.out.println("Error looking up JNDI resource [{}].");
            throw e;
        }

        System.out.println(response);
        Assert.assertNotNull(response);

//        classUnderTest = new JndiLookup();
//
//        final String responseString = classUnderTest.lookup(null, "ldap://localhost:6389/dc=myorg,dc=com");
//
//        Assert.assertEquals("X", responseString);
    }

}
